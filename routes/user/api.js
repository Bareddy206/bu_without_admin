const router = require('express').Router();

const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const randomstring = require("randomstring");

const config = require('../../config/config');
const User = require('../../models/user');
const Product = require('../../models/product');
const Storename = require('../../models/storename');

const encrypt = require('../../methods/encrypt');
const sendMail = require('../../methods/email');
const statusMsg = require('../../methods/statusMsg');

var tableName = require('../../config/table');
const passport = require('../../config/passport');
const rand = require('csprng');
const crypto=require('crypto');
const cryptoJs= require('crypto-js');

const flash = require('connect-flash');

const mailgun = require('mailgun-js')({
	apiKey: config.API_KEY_MAILGUN,
	domain: config.MAILGUN_DOMAIN
});
router.get('/', (req, res) => {
	res.render('admin2/signin');
  });
  router.get('/signup', (req, res) => {
	  res.render('admin2/signup');
  });
router.post('/signup1',(req,res)=>{
	console.log("suma");
	var fname=req.body.fname;
    var lname=req.body.lname;
	var email=req.body.email;
	var pwd=req.body.pwd;
	var cpwd=req.body.cpwd;
        console.log(fname+" " + lname+ "  " + email);
        res.render('admin2/store_details',{  
            fname:fname,
            lname:lname,
			email:email,
			pwd:pwd,
			cpwd:cpwd
        });
});
router.get('/product',(req,res)=>{
	res.locals.SKU=Date.now();
	res.render('admin2/store_setup_product');

});
router.get('/basket',(req,res)=>{
	res.locals.SKU=Date.now();
	var storeName=req.query.storeName;
	console.log("basket:" + storeName);
	Product.selectTable(storeName+'_store_data');
	Product.query('product', (err, productItems) => {
		//total cart count
		const json=JSON.stringify(productItems);
		console.log("products: "+json);
		res.locals.products = productItems || {};
		res.locals.productCount = productItems.length || 0;
		//res.render('index');
		res.render('admin2/store_setup_basket');
	});
	

});
router.get('/plan',(req,res)=>{
	res.locals.SKU=req.query.sku;
	res.render('admin2/store_setup_plan');

});
router.get('/payment',(req,res)=>{
	res.render('admin2/store_setup_payment');

});
router.get('/store_setup',(req,res)=>{
	res.render('admin2/store_initialise');

});


router.post('/account_details',(req,res)=>{
	var MID=req.body.MID;
	console.log("suma Testing:"+ MID);
	var MERCHANT_KEY=req.body.MERCHANT_KEY;
	const secret = '#phoenix@haroz@1465';
	// Encrypt 
	const encrypted_mid = cryptoJs.AES.encrypt(MID, secret);
	const encrypted_merchant_key=cryptoJs.AES.encrypt(MERCHANT_KEY, secret);
	console.log('encryped data:\n'+encrypted_mid+'\n'+encrypted_merchant_key);
	// Decrypt 
	var bytes  = cryptoJs.AES.decrypt(encrypted_mid.toString(), secret);
	var plaintext = bytes.toString(cryptoJs.enc.Utf8);
	var mid=encrypted_mid.toString();
	var merchant_key=encrypted_merchant_key.toString();
	console.log("decrypted data:"+ plaintext);
	const store=req.query.storeName;
	console.log("storeName:"+ store);
	
			const putParams={
				storeName:store,
				MID:mid,
				MERCHANT_KEY:merchant_key
			};
			console.log("putParams:"+ putParams.MID);
			Storename.selectTable('store_details');
			Storename.updateItem(putParams,{},(err,item)=>{
				if (err) {
					res.send(statusMsg.errorResponse(err));
					console.error("Unable to update item. Error JSON:", err);
				}
				console.log("UpdateItem succeeded:");
				 res.send({
				 	status: 'success',
				 	message: 'Payment configuration is done'
				 });


			});
});
//Signup

/*router.post('/signup', (req, res, next) => {
	//console.log('SumaTesting'+req.email);
	passport.authenticate("local-signup", {
		successRedirect: '/signin',
		failureRedirect: '/',
		successFlash: true,
		failureFlash: true,
	})(req, res, next)
}, (req, res) => {});*/
router.post('/signup', (req, res) => {
	const bodyParams = req.body;
	console.log('bodyParams: ', bodyParams);
	const name = bodyParams.storeName;
	const userEmail=bodyParams.email;
	const rootUrl = `${req.protocol}://${req.get('host')}`; //equates to http :// localhost or ec2hostname
	if (bodyParams.password !== bodyParams.cpassword) {
		res.send(`Password doesn't match`);
	}
	//select table before
	 Storename.selectTable('store_details');
	 Storename.getItem(name, {}, (err, name) => {
		console.log(name, err);
		if (err) {
			return res.send(statusMsg.errorResponse(err))
		} if (Object.keys(name).length === 0) {
			console.log('storename',name);
			User.selectTable('Merchant_details');
	        User.getItem(userEmail, {}, (err, user) => {
				console.log(user, err);
				if (err) {
					return res.send(statusMsg.errorResponse(err))
				} if (Object.keys(user).length === 0) {
					console.log('user',user);
					//res.send(user)
					const createCallback = (hashnewpwd) => {
						const verification_code = randomstring.generate();
						const putParams = {
							"email": userEmail,
							"username": bodyParams.username,
							"storeName": bodyParams.storeName,
							"password": hashnewpwd,
							"verification_code": verification_code,
							"verifiedornot": 'no',
						};
						console.log("Adding a new item...\n", putParams);
						User.createItem(putParams, {table:'Merchant_details', overwrite: false }, (err, user) => {
							if (err) {
								res.send(statusMsg.errorResponse(err));
							} else {
								console.log('\nAdded\n', user);
								const params = {"storeName": bodyParams.storeName };
								Storename.createItem(params, {table:'store_details', overwrite: false }, (err, name) => {
									if (err) {
										res.send(statusMsg.errorResponse(err));
									} else {
										console.log('\nAdded\n', name);
										// const verification_code = randomstring.generate();
								const mailData = sendMail.sendVerificationMail(userEmail,bodyParams.storeName, verification_code,rootUrl);
								mailgun.messages().send(mailData, (err, body) => {
									if (err) {
										res.send(statusMsg.errorResponse(err));
										//req.flash('error', 'Failed!');
										console.log('err: ', err);
									} else {
										//req.flash('success', 'Registered! Verify your mailid');
										res.send({
											status: "success",
											message: "email sent to your mailid"
										});
										//res.redirect('/signin');
									}
								});
									}
								});
							}
						});
					}
					encrypt.generateSalt(res, bodyParams.password, createCallback);
					
				}
				if (Object.keys(user).length > 0) {
					res.send({
						status: 'failure',
						message: 'emailId already taken'
					})
				}
	       })
		}
		if (Object.keys(name).length > 0) {
			res.send({
				status: 'failure',
				message: 'storename already taken'
			})
		}
	})
});

/*router.post('/login',
	passport.authenticate("local-login", {
		successRedirect: '/store_setup',
		failureRedirect: 'back',
		successFlash: true,
		failureFlash: true,
	}), (req, res) => {
	});*/

//Login
router.post('/login', (req, res) => {
	const bodyParams = req.body;
	console.log('bodyParams: ', bodyParams);
	const userEmail = bodyParams.email;
	User.selectTable('Merchant_details');
	User.getItem(userEmail, {}, (err, user) => {
		console.log('user: ', user);
		if (err) {
			res.send(statusMsg.errorResponse(err));
		} else if (Object.keys(user).length === 0) {
			res.send(statusMsg.incorrectResponse('user'));
		}
		const hashPassword = user.password;
		bcrypt.compare(bodyParams.password, hashPassword, (err, result) => {
			if (err) {
				res.send(statusMsg.errorResponse(err));
			}
			if (result) {
				console.log(result);
				token= jwt.sign({
					email: `${userEmail}`,
					storeName: `${user.storeName}`,
					exp: Math.floor(Date.now() / 1000) + (60 * 60)
				}, config.SECRET_KEY)
				res.send({ 
					"status":'Successfully logged in!', 
					"token": token , 
					"storeName":`${user.storeName}`});
				res.locals.storeName=`${user.storeName}`;
				// res.render('admin2/store_initialise',{
				// 	token:token
				// });
			} else {
				res.send(statusMsg.incorrectResponse('password'));
			}
		})
	})
});
router.post('/emailcheck',(req,res)=>{
	var email=req.body.email;
	console.log("emailcheck:" + email);
	User.selectTable('Merchant_details');
	User.getItem(email,{},(err,user)=>{
		if(err)
		{
			res.send(statusMsg.errorResponse(err));
		}else if (Object.keys(user).length === 0) {
			res.send({
				status:"OK",
				message:"you can procced"
			});
		}else
		{
			res.send({
				status:"failure",
				message:"email already taken"
			});
		}
	});


});
router.post('/storecheck',(req,res)=>{
	var storeName=req.body.storeName;
	console.log("storeNamecheck:" + storeName);
	Storename.selectTable('store_details');
	Storename.getItem(storeName,{},(err,name)=>{
		if(err)
		{
			res.send(statusMsg.errorResponse(err));
		}else if (Object.keys(name).length === 0) {
			res.send({
				status:"OK",
				message:"you can procced"
			});
		}else
		{
			res.send({
				status:"failure",
				message:"storeName already taken"
			});
		}
	});


});

router.get('/logout', (req, res) => {
	//req.logOut();
	//req.flash('success', 'Successfully Logged out!');
	//customRedirect(req, res);
	res.redirect('/');
});

//Signup Verification
router.get('/signupverification', (req, res) => {
	const queryParams = req.query;
	console.log(queryParams);
	const userEmail = queryParams.email;
	User.selectTable('Merchant_details');
	User.getItem(userEmail, {}, (err, user) => {
		console.log('email checking');
		console.log(queryParams.email);
		if (err) {
			console.log('\nerr', err);
			// console.log('');
			res.send(statusMsg.errorResponse(err));
		} else if (Object.keys(user).length === 0) {
			console.log('\nNo user');
			res.send({
				status: 'failure',
				message: 'EmailId and token do not match'
			})
		} else if (Object.keys(user).length > 0) {
			console.log('In Signup verification');
			console.log(user.verification_code,queryParams.verification_code);
			if (user.verification_code === queryParams.verification_code) {
				const updateParams = {
					email: userEmail,
					verifiedornot: 'yes'
				}
				console.log("Updating the item...");
				User.updateItem(updateParams, {}, (err, data) => {
					if (err) {
						res.send(statusMsg.errorResponse(err));
						console.error("Unable to update item. Error JSON:", JSON.stringify(err, null, 2));
					} else {
						console.log("UpdateItem succeeded:", JSON.stringify(data, null, 2));

						var res1 = tableName.tempcreateTables(req.query.storeName, {
						}, (err, table) => {
							if (err) {
								console.log(err);
							} else {
								console.table("this is " + table);
							}
						});
						//res.send(statusMsg.verifySuccess)
						res.redirect('/');
					}
				});
			} else {
				res.send({
					status: 'failure',
					message: 'auth code is not correct'
				});
			}
		}
	})
});
router.get('/forgotpassword',(req,res)=>{
	res.render('admin2/reset_pass_mail');

});

// Forgot Password
router.post('/forgotpwd', (req, res) => {
	const bodyParams=req.body;
	const userEmail = req.body.email;
	const rootUrl = `${req.protocol}://${req.get('host')}`; //equates to http :// localhost or ec2hostname
	User.selectTable('Merchant_details');
	User.getItem(userEmail,{},(err,user)=>{
		console.log('email checking');
		console.log(bodyParams.email);
		if (err) {
			console.log('\nerr', err);
			// console.log('');
			res.send(statusMsg.errorResponse(err));
		} else if (Object.keys(user).length === 0) {
			console.log('\nNo user');
			res.send({
				status: 'failure',
				message: 'User not Exist'
			})
		} else if(Object.keys(user).length > 0){
			const temp =rand(24, 24);
			//user.temp_str=temp;
			const updateParams = {
				email: userEmail,
				temp_str: temp
			};
			console.log("Updating the item...");
			User.updateItem(updateParams, {}, (err, data) => {
				if (err) {
					//res.send(statusMsg.errorResponse(err));
					console.error("Unable to update item. Error JSON:", JSON.stringify(err, null, 2));
				}
				console.log("UpdateItem succeeded:", JSON.stringify(data, null, 2));
				// res.send({
				// 	status: 'success',
				// 	message: 'updated successfully'
				// })

			});
			const dataToSend = sendMail.forgotPasswordMail(userEmail,rootUrl,temp);
			mailgun.messages().send(dataToSend, (error, body) => {
				if (error) {
					res.send(statusMsg.errorResponse(err));
				} else {
					console.log("email sent");
					console.log('\nbody\n',body);
					// return done(null, { userDetails: params.Item, message: 'email send to your mail' });
					res.send({
						status: "success",
						message: "email sent to your mailid"
					});
				}
			})
	}

	})
});

// Update Password
router.post('/updatepwd', (req, res) => {
	const bodyParams = req.body;
	const userEmail = bodyParams.email;
	User.selectTable('Merchant_details');
	User.getItem({ email: userEmail }, {}, (err, user) => {
		console.log('email checking');
		console.log(Object.keys(user).length);
		// const hashPassword = user.Item.password;
		if (err) {
			res.send(statusMsg.errorResponse(err));
		} else if (Object.keys(user).length === 0) {
			res.send(statusMsg.incorrectResponse('user'));
		} else {
			const updateCallback = function (hashnewpwd) {
				const updateParams = {
					email: userEmail,
					password: hashnewpwd
				};
				console.log("Updating the item...");
				//User.selectTable('Merchant_details');
				User.updateItem(updateParams, {}, (err, data) => {
					if (err) {
						res.send(statusMsg.errorResponse(err));
						console.error("Unable to update item. Error JSON:", JSON.stringify(err, null, 2));
					}
					console.log("UpdateItem succeeded:", JSON.stringify(data, null, 2));
					// res.send({
					// 	status: 'success',
					// 	message: 'updated successfully'
					// })
					res.redirect('/');

				});

			};
			encrypt.generateSalt(res, bodyParams.newpassword, updateCallback);
		}

	});
});

router.get('/resetpassword',(req,res)=>{
	var email=req.query.email;
	console.log("email :" + email);
	var code=req.query.code;
	res.render('admin2/change_password',{email:email});

});

// Reset password
router.post('/resetpwd', (req, res) => {
	const bodyParams = req.body;
	const userEmail = bodyParams.email;
	User.selectTable('Merchant_details');
	User.getItem(userEmail, {}, (err, user) => {
		console.log('email checking', Object.keys(user).length);
		if (err) {
			res.send(statusMsg.errorResponse(err));
		} else if (Object.keys(user).length === 0) {
			res.send(statusMsg.incorrectResponse('user'));
		}
		const hashPassword = user.password;
		console.log(hashPassword)
		bcrypt.compare(bodyParams.password, hashPassword, (err, result) => {
			if (err) {
				console.log('\nerr: ', err);
				res.send(statusMsg.errorResponse(err));
			} else if (result) {
				console.log('\nresult: ', result);
				// var token_info = {token: jwt.sign({ email: 'harishkashaboina94@gmail.com', exp: Math.floor(Date.now() / 1000) + (60 * 60)}, 'cadenza')};
				const updateCallback = (hashnewpwd) => {
					const updateParams = {
						"email": userEmail,
						"password": hashnewpwd
					};
					User.updateItem(updateParams, {}, (err, data) => {
						if (err) {
							res.send(statusMsg.errorResponse(err))
							console.error("Unable to update item. Error JSON:", JSON.stringify(err, null, 2));
						}
						console.log("UpdateItem succeeded:", JSON.stringify(data, null, 2));
						//res.send(statusMsg.verifySuccess)
						res.redirect("/admin/signin");
					});
				}
				encrypt.generateSalt(res, bodyParams.newpassword, updateCallback);
			} else {
				res.send(statusMsg.incorrectResponse('password'));
			}
		})

	});
});

module.exports = router;