const express = require('express');
const path = require('path');
const logger = require('morgan');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const flash = require('connect-flash');
const passport = require('passport');
var session = require('express-session');
const methodOverride = require('method-override');
const config = require('./config/config');
const DynamoDBStore = require('dynamodb-store');
const jwt = require('jsonwebtoken');
const subdomain= require('express-subdomain');

const app = express();
const router = express.Router();

const minute = 60000;
const hour = 3600000;
const day = 86400000;
const options = {
    "table": {
        "name":"test-sessions",
        "hashPrefix": '',
    },
    "dynamoConfig": {
        "accessKeyId": config.ACCESS_KEY_ID,
        "secretAccessKey": config.SECRET_ACCESS_KEY,
        "region": 'ap-south-1'
    },
    "keepExpired": false,
};

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(cookieParser());

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');   
//Flash messages
//app.use(flash()); 

app.use(express.static(path.join(__dirname, 'public')));
app.use(methodOverride('_method'));
// app.use(session({ //will work with everything below this so declare your static assets above
//     store: new DynamoDBStore(options),
//     secret: config.SESSION_SECRET,
//     resave: false,
//     saveUninitialized: false,
//     cookie: {
//         httpOnly: true, //only allows cookie access to server
//         maxAge: hour
//     }
// }));

///passport config
//app.use(passport.initialize());
//app.use(passport.session()); //At this point of time flash messages are not working; Have to revisit this later for debugging


//Middleware
app.use('/', router);  
router.use((req, res, next)=> {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE');
    // res.locals.currentUser = (req.user) ? req.user: undefined;
    // res.locals.success = req.flash('success');
    // res.locals.error = req.flash('error');
    // if(req.user!==undefined){
    //     var token= jwt.sign({
    //         email: `${req.user.email}`,
    //         storeName: `${req.user.storeName}`,
    //         exp: Math.floor(Date.now() / 1000) + (60 * 60)
    //     }, config.SECRET_KEY);
    //     res.locals.currentUser.token=token;
    //     console.log('token:\n',res.locals.currentUser.token);
    // }
    //console.log('\ncurrent User:\n',res.locals.currentUser);
    //console.log('\nlocal success\n',res.locals.success);
    next();
});

//Route variables
//const index = require('./routes/index');
// const products= require('./routes/product/api');
// const plans = require('./routes/plans/api');
// const orders= require('./routes/order/api');
// const cart= require('./routes/cart/api');
const users= require('./routes/user/api');
//Routing
//app.use('/',index);
// app.use('/api/products',products);
// app.use('/api/plans',plans);
// app.use('/api/orders',orders);
// app.use('/api/cart',cart);
//app.use('/',index);
app.use('/',users);
//app.use(subdomain('admin', users));
module.exports = app;
